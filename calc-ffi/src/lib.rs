use calc::prelude::*;
use std::ffi::CStr;
use std::os::raw::c_char;

// What I want to expose:
// * single function
// * takes a C string of "input"
// * returns (if successful) the output

// parse_and_eval
//
// Arguments?
//    * const char
// Return type?
//    * integer

// returns 0 if success, returns nonzero if failure
// on success, output is updated with the result?
//
// int parse_and_eval(char*, int64_t* output);

pub use calc::Expr;

#[no_mangle]
pub extern "C" fn parse_and_eval(maybe_cstr: *const c_char, output: *mut i64) -> isize {
    let expr = c_parse(maybe_cstr);
    if expr.is_null() {
        return -1;
    }

    c_eval(expr, output)
}

/// This will return null if unsuccessful
#[no_mangle]
pub extern "C" fn c_parse(maybe_cstr: *const c_char) -> *mut Expr {
    // Check if cstr is valid
    if maybe_cstr.is_null() {
        return std::ptr::null_mut();
    }
    let cstr = unsafe { CStr::from_ptr(maybe_cstr) };
    let string = match cstr.to_str() {
        Ok(s) => s,
        Err(_) => return std::ptr::null_mut(),
    };

    match parse(string) {
        Ok(p) => Box::leak(Box::new(p)) as *mut Expr,
        Err(_) => std::ptr::null_mut(),
    }
}

#[no_mangle]
pub extern "C" fn c_eval(expr: *const Expr, output: *mut i64) -> isize {
    if expr.is_null() {
        return -1;
    }

    if output.is_null() {
        return -1;
    }

    let result = match eval(unsafe { &*expr }) {
        Ok(result) => result,
        Err(_) => return -1,
    };

    unsafe { *output = result };

    0
}

#[no_mangle]
pub extern "C" fn release_expr(box_expr: *mut Expr) {
    if !box_expr.is_null() {
        unsafe {
            Box::from_raw(box_expr);
        }
    }
}
