#![allow(dead_code)]
#[derive(PartialEq, Debug)]
enum FarmAnimal {
    Worm,
    Cow,
    Bull,
    Chicken { num_eggs: usize },
    Dog { name: String },
}

fn what_does_the_animal_say(animal: &FarmAnimal) {

    let noise = match animal {
        FarmAnimal::Cow | FarmAnimal::Bull => "moo".to_string(),
        FarmAnimal::Chicken { .. } => "cluck, cluck!".to_string(),
        FarmAnimal::Dog { name } =>  {
            if name == "Lassie" {
                format!("woof, woof! I am a LADY!")
            } else {
                format!("woof, woof! I am {}!", name)
            }
        },
        _ => "-- (silence)".to_string(),
    };


    println!("{:?} says: {:?}", animal, noise);
}

fn main() {
    what_does_the_animal_say(
        &FarmAnimal::Dog {
            name: "Lassie".to_string()
    });
    what_does_the_animal_say(&FarmAnimal::Cow);
    what_does_the_animal_say(&FarmAnimal::Bull);
    what_does_the_animal_say(&FarmAnimal::Chicken{num_eggs: 3});
}
