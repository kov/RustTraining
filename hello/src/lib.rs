use std::ffi::CStr;
use libc::c_char;

#[no_mangle]
pub unsafe extern "C" fn say(s: *const c_char) {
    let cstr = match  CStr::from_ptr(s).to_str() {
        Ok(cstr) => cstr,
        Err(e) => panic!("Failed: {}", e),
    };
    println!("{}", cstr)
}

#[repr(C)]
pub enum MyPrintResult {
    MyPrintResultOK,
    MyPrintResultError,
}

#[no_mangle]
pub extern "C" fn myprint(data: *const c_char) -> MyPrintResult {
    if data.is_null() {
        return MyPrintResult::MyPrintResultOK;
    }

    let cstr = unsafe { CStr::from_ptr(data) };

    match cstr.to_str() {
        Ok(rstr) => println!("{}", rstr),
        Err(_) => return MyPrintResult::MyPrintResultError,
    }

    MyPrintResult::MyPrintResultError
}

#[cfg(test)]
mod tests {
    use std::ffi::CString;
    use super::*;

    #[test]
    fn it_works() {
        let cstring = CString::new("test").unwrap();
        say(cstring.as_ptr());
    }
}
