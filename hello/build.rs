extern crate cbindgen;

use std::env;

fn main() {
    let crate_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    cbindgen::Builder::new()
      .with_crate(&crate_dir)
      .with_language(cbindgen::Language::C)
      .generate()
      .expect("Unable to generate bindings")
      .write_to_file("hello.h");

    cbindgen::Builder::new()
      .with_crate(crate_dir)
      .with_language(cbindgen::Language::Cxx)
      .generate()
      .expect("Unable to generate bindings")
      .write_to_file("hello.hpp");
}
