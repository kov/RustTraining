use std::ffi::CStr;
use std::os::unix::ffi::OsStrExt;
use std::path::PathBuf;
use leveldb_sys::*;
use libc::c_char;

pub enum OpenFlags {
    None,
    CreateIfMissing,
}

pub struct OpenOptions(std::ptr::NonNull<leveldb_options_t>);

impl OpenOptions {
    pub fn new(flags: impl Into<Option<OpenFlags>>) -> Self {
        unsafe {
            let options = leveldb_options_create();
            if let Some(OpenFlags::CreateIfMissing) = flags.into() {
                leveldb_options_set_create_if_missing(options, 1);
            }
            OpenOptions(std::ptr::NonNull::new(options).unwrap())
        }
    }
}

impl Drop for OpenOptions {
    fn drop(&mut self) {
        unsafe { leveldb_options_destroy(self.0.as_ptr()) }
    }
}

pub struct DB(std::ptr::NonNull<leveldb_t>);

impl DB {
    pub fn new(path: &PathBuf, options: &OpenOptions) -> Self {
        unsafe {
            let name = path.as_os_str().as_bytes().as_ptr() as *const c_char;

            let mut error = std::ptr::null_mut();
            let db = leveldb_open(
                options.0.as_ptr(),
                name,
                &mut error
            );

            if !error.is_null() {
                let error_string = CStr::from_ptr(error as *const c_char);
                panic!("Failed to open the database named {}: {}", path.to_string_lossy(), error_string.to_str().unwrap());
            }

            DB(std::ptr::NonNull::new(db).unwrap())
        }
    }
}

impl Drop for DB {
    fn drop(&mut self) {
        unsafe { leveldb_close(self.0.as_ptr()) }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let path = PathBuf::from("/tmp/leveldb-rs-test");
        let options = OpenOptions::new(OpenFlags::CreateIfMissing);

        // Make sure we start from a clean slate.
        let _ = std::fs::remove_dir_all(path.as_path());


        // Try creating the directory, this should work.
        let db = DB::new(&path, &options);
        drop(db);

        // Try openning the existing db
        let _ = DB::new(&path, &options);
     }

    #[test]
    #[should_panic]
    fn it_panics() {
        // Try creating a new db where we should not be able to write
        let path = PathBuf::from("/leveldb-rs-test");
        let options = OpenOptions::new(OpenFlags::CreateIfMissing);
        let _ = DB::new(&path, &options);
     }
}
